﻿using System;
using UnityEngine;

public class GameState : UnitySingleton<GameState>, INotify
{
    public event EventHandler<NotifyChangedEventArgs> NotifyChanged;

    public int Health { get; set; }
    public int Points { get; set; }
    public float Time { get; set; }
    public string Username { get; set; }

    public void Load()
    {
        Username = Global.Username;
        Health = Global.Health;
        Points = Global.Points;
    }

    public void Persist()
    {
        Global.Health = Health;
        Global.Points = Points;
        Global.Time = Time;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void BeforeSceneLoadHook()
    {
        Initialize();
    }
}
