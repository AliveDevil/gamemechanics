﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioState : Singleton<AudioState>
{
    private GameConfig gameConfig;
    public AudioMixer Mixer { get; set; }

    public void Apply()
    {
        gameConfig = GameConfig.Instance;
        ApplyAll();
        gameConfig.NotifyChanged += GameConfig_NotifyChanged;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void BeforeSceneLoadHook()
    {
        Initialize();
    }

    private void ApplyAll()
    {
        ApplyMusicVolume();
        ApplySoundEffectVolume();
    }

    private void ApplyMusicVolume()
    {
        Mixer.SetFloat("musicVolume", gameConfig.MusicVolume);
    }

    private void ApplySoundEffectVolume()
    {
        Mixer.SetFloat("soundeffectVolume", gameConfig.SoundEffectVolume);
    }

    private void GameConfig_NotifyChanged(object sender, NotifyChangedEventArgs e)
    {
        if (string.IsNullOrWhiteSpace(e.Property))
        {
            ApplyAll();
        }
        else if (e.Property == "MusicVolume")
        {
            ApplyMusicVolume();
        }
        else if (e.Property == "SoundEffectVolume")
        {
            ApplySoundEffectVolume();
        }
    }
}
