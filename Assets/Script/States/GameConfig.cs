﻿using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;

public class GameConfig : MonoBehaviour, INotify
{
    private float musicVolume;
    private bool runtimeCreated;
    private float soundeffectVolume;

    public event EventHandler<NotifyChangedEventArgs> NotifyChanged;

    public static GameConfig Instance { get; private set; }

    public float MusicVolume
    {
        get { return musicVolume; }
        set
        {
            if (musicVolume != value)
            {
                musicVolume = value;
                NotifyChanged?.Invoke(this, new NotifyChangedEventArgs(nameof(MusicVolume)));
            }
        }
    }

    public float SoundEffectVolume
    {
        get { return soundeffectVolume; }
        set
        {
            if (soundeffectVolume != value)
            {
                soundeffectVolume = value;
                NotifyChanged?.Invoke(this, new NotifyChangedEventArgs(nameof(SoundEffectVolume)));
            }
        }
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Setup()
    {
        var g = new GameObject("Game Config");
        DontDestroyOnLoad(g);
        g.SetActive(false);
        var config = g.AddComponent<GameConfig>();
        config.runtimeCreated = true;
        Instance = config;
        g.SetActive(true);
    }

    private void Load()
    {
        var path = Application.persistentDataPath + "/config.bin";
        var fileInfo = new FileInfo(path);
        if (!fileInfo.Exists)
        {
            return;
        }
        JObject root;
        using (var stream = fileInfo.OpenRead())
        using (var binaryReader = new BinaryReader(stream))
        using (var reader = new BsonDataReader(binaryReader))
        {
            root = JToken.ReadFrom(reader).Value<JObject>();
        }

        var volumes = root.GetValue("volumes").Value<JObject>();
        musicVolume = volumes.GetValue("music").Value<float>();
        soundeffectVolume = volumes.GetValue("effects").Value<float>();
    }

    private void OnDisable()
    {
        if (runtimeCreated)
        {
            JObject root = new JObject();

            var volumes = new JObject
            {
                { "music", musicVolume },
                { "effects", soundeffectVolume }
            };

            root.Add("volumes", volumes);

            var path = Application.persistentDataPath + "/config.bin";
            var fileInfo = new FileInfo(path);

            if (!fileInfo.Directory.Exists)
            {
                fileInfo.Directory.Create();
            }

            using (var stream = fileInfo.Open(FileMode.Create, FileAccess.Write, FileShare.None))
            using (var binaryWriter = new BinaryWriter(stream))
            using (var writer = new BsonDataWriter(binaryWriter))
            {
                root.WriteTo(writer);
            }
        }
    }

    private void OnEnable()
    {
        if (!runtimeCreated)
        {
            Destroy(gameObject);
        }

        Load();
    }
}
