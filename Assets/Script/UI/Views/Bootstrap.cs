﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

public class Bootstrap : View
{
    [SerializeField]
    private AudioMixer mixer = null;

    [SerializeField]
    private View nextView = null;

    private IEnumerator Start()
    {
        AudioState.Instance.Mixer = mixer;
        yield return null;
        AudioState.Instance.Apply();
        yield return null;
        ViewStack.Replace(nextView);
    }
}
