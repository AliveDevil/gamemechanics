﻿using UnityEngine;
using UnityEngine.UI;

public class PrepareGame : View
{
    [SerializeField]
    private InputField usernameInput = null;

    public void Close()
    {
        ViewStack.Pop();
    }

    public void StartGame()
    {
        GameState.Global.Username = usernameInput.text;
    }

    private void OnEnable()
    {
        usernameInput.text = GameState.Global.Username;
    }
}
