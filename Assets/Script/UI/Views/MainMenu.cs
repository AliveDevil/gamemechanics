﻿using System;
using UnityEngine;

public class MainMenu : View
{
    [SerializeField]
    private ViewConfig config;

    public void Credits()
    {
        ViewStack.Push(config.Credits);
    }

    public void Highscore()
    {
        ViewStack.Push(config.Highscore);
    }

    public void Options()
    {
        ViewStack.Push(config.Config);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        ViewStack.Push(config.GameView);
    }

    [Serializable]
    private struct ViewConfig
    {
        public View Config;
        public View Credits;
        public View GameView;
        public View Highscore;
    }
}
