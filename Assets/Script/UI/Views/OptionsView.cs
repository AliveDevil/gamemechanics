﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsView : View
{
    private GameConfig gameConfig;
    [SerializeField] private Slider musicSlider = null;
    [SerializeField] private Slider soundEffectSlider = null;

    public void Close()
    {
        ViewStack.Pop();
    }

    public void MusicVolume(float value)
    {
        gameConfig.MusicVolume = value;
    }

    public void SoundEffectVolume(float value)
    {
        gameConfig.SoundEffectVolume = value;
    }

    private void OnEnable()
    {
        gameConfig = GameConfig.Instance;

        musicSlider.value = gameConfig.MusicVolume;
        soundEffectSlider.value = gameConfig.SoundEffectVolume;
    }
}
