﻿using UnityEngine;

public abstract class View : MonoBehaviour
{
    public ViewStack Root
    {
        get
        {
            var stack = ViewStack;
            while (stack.Parent)
            {
                stack = stack.Parent;
            }
            return stack;
        }
    }

    public ViewStack ViewStack { get; set; }
}
