﻿using System.Collections.Generic;
using UnityEngine;

public class ViewStack : MonoBehaviour
{
    [SerializeField]
    private Transform content = null;

    [SerializeField]
    private View initialView = null;

    [SerializeField]
    private View rootView = null;

    private Stack<View> views = new Stack<View>();

    public Transform Content
    {
        get { return content; }
        set { content = value; }
    }

    public ViewStack Parent => rootView ? rootView.ViewStack : null;

    public View RootView
    {
        get { return rootView; }
        set { rootView = value; }
    }

    public View View { get; private set; }

    public void Pop()
    {
        EnableLast();
    }

    public T Push<T>(T viewPrefab) where T : View
    {
        DisableLast();
        return SetCurrentView(viewPrefab);
    }

    public T Replace<T>(T viewPrefab) where T : View
    {
        return SetCurrentView(viewPrefab);
    }

    private void DestroyCurrentView()
    {
        if (View)
        {
            Destroy(View.gameObject);
        }
    }

    private void DisableLast()
    {
        View.gameObject.SetActive(false);
        views.Push(View);
        View = null;
    }

    private void EnableLast()
    {
        DestroyCurrentView();

        View = views.Pop();
        View.gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        SetCurrentView(initialView);
    }

    private T SetCurrentView<T>(T viewPrefab) where T : View
    {
        DestroyCurrentView();

        var tempGameObject = new GameObject();
        tempGameObject.SetActive(false);
        var instance = Instantiate(viewPrefab, tempGameObject.transform, false);
        instance.gameObject.SetActive(false);
        instance.transform.SetParent(content, false);
        Destroy(tempGameObject);
        instance.ViewStack = this;
        View = instance;
        instance.gameObject.SetActive(true);
        return instance;
    }
}
