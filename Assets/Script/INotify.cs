﻿using System;

public interface INotify
{
    event EventHandler<NotifyChangedEventArgs> NotifyChanged;
}

public sealed class NotifyChangedEventArgs : EventArgs
{
    private readonly string _property;

    public NotifyChangedEventArgs(string property)
    {
        _property = property;
    }

    public string Property
    {
        get
        {
            return _property;
        }
    }
}
