﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    public static T Instance { get; private set; }

    protected static void Initialize()
    {
        var g = new GameObject(typeof(T).Name);
        DontDestroyOnLoad(g);
        Instance = g.AddComponent<T>();
    }
}

public abstract class UnitySingleton<T> : MonoBehaviour where T : UnitySingleton<T>
{
    private readonly static Dictionary<int, T> perSceneInstances = new Dictionary<int, T>();
    private static int activeScene;
    private static T global;

    public static T Active
    {
        get
        {
            return PerScene(activeScene);
        }
    }

    public static T Global
    {
        get
        {
            if (!global)
            {
                var g = new GameObject(typeof(T).Name + " - Global");
                DontDestroyOnLoad(g);
                global = g.AddComponent<T>();
            }
            return global;
        }
    }

    public static T PerScene(GameObject gameObject)
    {
        var scene = gameObject.scene;
        if (scene == global.gameObject.scene)
        {
            return global;
        }
        var sceneIndex = scene.GetHashCode();
        T instance;
        if (perSceneInstances.TryGetValue(sceneIndex, out instance))
        {
            return instance;
        }

        var gameObjects = scene.GetRootGameObjects();
        foreach (var item in gameObjects)
        {
            var singleton = item.GetComponent<T>();
            if (singleton)
            {
                perSceneInstances[sceneIndex] = singleton;
                return singleton;
            }
        }

        var g = new GameObject(typeof(T).Name);
        SceneManager.MoveGameObjectToScene(g, scene);
        instance = g.AddComponent<T>();
        return instance;
    }

    protected static void Initialize()
    {
        Application.quitting += Application_quitting;
        SceneManager.activeSceneChanged += SceneManager_activeSceneChanged;
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
    }

    private static void Application_quitting()
    {
        Application.quitting -= Application_quitting;
        SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
        SceneManager.activeSceneChanged -= SceneManager_activeSceneChanged;
    }

    private static T PerScene(int scene)
    {
        T instance;
        if (!perSceneInstances.TryGetValue(scene, out instance))
        {
            var g = new GameObject(typeof(T).Name);
            instance = g.AddComponent<T>();
            perSceneInstances[scene] = instance;
        }
        return instance;
    }

    private static void SceneManager_activeSceneChanged(Scene current, Scene next)
    {
        activeScene = next.GetHashCode();
    }

    private static void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Do not do nothing do do.
    }

    private static void SceneManager_sceneUnloaded(Scene scene)
    {
        int sceneIndex = scene.GetHashCode();
        T instance;
        if (perSceneInstances.TryGetValue(sceneIndex, out instance))
        {
            perSceneInstances.Remove(sceneIndex);
        }
    }

    private void OnEnable()
    {
        perSceneInstances[gameObject.scene.GetHashCode()] = (T)this;
    }
}
